<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimoni extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table = '_testimoni';
    protected $guarded = [];
    protected $primaryKey = 'id_testimoni';

    function User(){
        return $this->belongsTo(User::class,'nisn');
    }

    function Pengajuan(){
        return $this->belongsTo(Pengajuan::class,'id_pengajuan');
    }
}
