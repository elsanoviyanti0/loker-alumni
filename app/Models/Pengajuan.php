<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    use HasFactory;
    protected $table = "pengajuan";
    protected $primaryKey = "id_pengajuan";
    protected $guarded = [];

    function User()
    {
        return $this->belongsTo(User::class, 'nisn');
    }
    function Posting(){
        return $this->belongsTo(Posting::class,'id_posting');
    }
}
