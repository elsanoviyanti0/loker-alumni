<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Model;

class Admin extends Model
{
    use HasFactory;
    protected $table = "admin";
    protected $guarded = [];
    protected $primaryKey = 'id_admin';

    function posting(){
        return $this->hasMany(Admin::class,'id_admin');
    }
}
