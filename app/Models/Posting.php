<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Events\TransactionBeginning;

class Posting extends Model
{
    use HasFactory;
    protected $table = "posting";
    protected $guarded = [];

    function User()
    {
        return $this->belongsTo(User::class, 'nisn');
    }

    function Admin(){
        return $this->belongsTo(Admin::class,'id_admin');
    }
    function perusahaan(){
        return $this->belongsTo(Perusahaan::class,'id_perusahaan');
    }
    function Posting(){
        return $this->hasMany(Posting::class,'id_posting');
    }
}
