<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\Models\Alumni;

class AlumniController extends Controller
{
    //
    function register(){
        return view('footer.register');
    }
    function create(Request $req){
        $validate = $this->validate($req,[
            'nisn' => 'required|string|max:20',
            'name' => 'required|string',
            'ktp' => 'required|string|min:10',
            'thn_lulus' => 'required|string',
            'foto' => 'required',
            'email' => 'required|string|min:8',
            'password' => 'required_with:confirm_password|min:8|same:confirm_password',
        ]);
        $validate['password']=bcrypt($req->password);
        Alumni::create($validate);
        return redirect ('footer.login');
    }
    function show(){
        $data['alumni'] = Alumni::all();
        return view('layout.landingpage');
    }
    function delete($id){
        Alumni::where('id_alumni', $id)->delete();
        return redirect('layout.landingpage');
    }

}
