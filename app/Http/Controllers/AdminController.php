<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\Cast\Object_;
use App\Models\Admin;

class AdminController extends Controller
{
    //
    public function show(){
      $data['admin'] = Admin::all();
      return view('layout.adm',$data);
    }

    function lyt(){
      return view('layout.tivo');
    }
    
    function add(){
      $data=[
        'action' =>url('admin/create'),
        'tombol' =>'Simpan',
        'admin' =>(Object)[
          'name' =>'',
          'foto' =>'',
          'email' =>'',
          'password' =>'',
        ]
      ];
      return view('alumni.form1',$data);
    }

    function create(Request $req){
      Admin::cretae([
        'name'=>$req->name,
        'foto'=>$req->foto,
        'email'=>$req->email,
        'password'=>$req->password
      ]);
      return redirect('alumni.index');
    }

    function hapus($id){
      $admin = Admin::where('id',$id)->delete();
      return redirect('alumni.index');

    }
    function edit($id){
      $data['admin'] = Admin::find($id);
      $data['action'] = url('admin/update'). '/' .$data['admin']->id;
      $data['tombol'] = 'Update';

      return view('alumni.form1',$data);
    }

    function update(Request $req){
      Admin::where('id',$req->id)->update([
        'name'=>$req->name,
        'foto'=>$req->foto,
        'email'=>$req->email,
        'password'=>$req->password
      ]);
      return redirect('alumni.index');
    }

    function login_adm(){
      return view('footer.login_adm');
    }

    function auth(Request $req){

      $credentials = $req->validate([
        'email' => ['required','email'],
        'password' => ['required'],
      ]);

      if(Auth::guard('admins')->attempt($credentials)){
        $req->session()->regenerate();
        return redirect('stisla');
        
      }else{
        return redirect('login/admin');
      }
  }

  function logout(){
    Auth::logout();
    return redirect('login/admin');
 }
}
