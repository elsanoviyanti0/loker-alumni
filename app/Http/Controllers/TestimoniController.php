<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Pengajuan;
use Illuminate\Http\Request;
use App\Models\Testimoni;
use Illuminate\Support\Facades\Auth;


class TestimoniController extends Controller
{
    //
    function index(){
        $data ['testimoni'] = Testimoni::orderBy('id_testimoni', 'desc')->get();
        return view('Testimoni.index', $data);
    }

    function delete($id){
        Testimoni::where('id_testimoni', $id)->delete();
        return redirect('testimoni');
    }

    function testimoni($siapa = ''){
        $testimoni = Testimoni::where('nisn', Auth()->guard('web')->id())->get();
        return view('Alumni.Testimoni.testimoni', ['testimoni' => $testimoni, 'siapa' => $siapa]);
    }

    function add_testimoni($id){
        $data ['pengajuan'] = Pengajuan::find($id);
        return view('Testimoni.form',$data);
    }

    function create_testimoni(Request $request){

        Testimoni::where('id_testimoni', $request->id)->create([
            'id_pengajuan' => $request->id_pengajuan,
            'nisn' => Auth::guard('web')->user()->nisn,
            'testimoni' => $request->testimoni
        ]);
        return redirect('landingpage')->with('status','Berhasil Menambahkan Testimoni!');
    }

}
