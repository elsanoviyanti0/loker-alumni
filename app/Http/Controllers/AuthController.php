<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Alumni;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    function login(){
        return view('footer.login');
    }

    function login_admin(){
        return view('footer.login_adm');
    }

    function auth(Request $req){
        $credentials = $req->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::guard('alumni')->attempt($credentials)){
            $req->session()->regenerate();
            return redirect('layout.landingpage');
        }

        if (Auth::guard('web')->attempt($credentials)){
            $req->session()->regenerate();
            return redirect('layout.dashbord');
        }
        
        return redirect('/');
    }
    function logout(){
        Auth::logout();
        return redirect('footer.login');
    }
    function logout_admin(){
        Auth::logout();
        return redirect('footer.login_adm');
    }
}

