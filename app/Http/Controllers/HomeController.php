<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Pengajuan;
use App\Models\Perusahaan;
use App\Models\Posting;
use App\Models\User;
use App\Models\Kontak;
use App\Models\Testimoni;

class HomeController extends Controller
{
    //
    function nav(){
      return view('layout.nav');
  }

  function home(){
   return view('footer.home');
}

    function show(){
        return view('layout.dashboard');
    }
     function admin(){
        return view('layout.admin');
     }

     function from(){
        return view('posting.from');
     }
     function index(){
      return view('posting.index');
   }

   function lay(){
      return view('layoute');
   }

   function das(){
      // $data['admin'] = Admin::all();
      $adminCount = Admin::count();
      $userCount = User::count();
      $pengajuanCount = Pengajuan::count();
      $perusahaanCount = Perusahaan::count();
      $postingCount = Posting::count();
      $kontakCount = Kontak::count();
      $testimoniCount = Testimoni::count();
  
      return view('layout.admin', [
          'adminCount' => $adminCount,
          'userCount' => $userCount,
          'pengajuanCount' => $pengajuanCount,
          'postingCount' => $postingCount,
          'perusahaanCount' => $perusahaanCount,
          'kontakCount' => $kontakCount,
          'testimoniCount' => $testimoniCount
      ]);
  }

  function profile(){
   // $data['user'] = User::find($nisn);
   return view('layout.profile');
  }

  
}
