<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Posting;
use App\Models\Perusahaan;

class PostingController extends Controller
{
    //

    public function postingList()
    { 
        $usersQuery = Posting::query();

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');

        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        if($start_date && $end_date){    

         $start_date = date('Y-m-d', strtotime($start_date));

         $end_date = date('Y-m-d', strtotime($end_date));       

         $usersQuery->whereRaw("date(users.created_at) >= '" . $start_date . "' AND date(users.created_at) <= '" . $end_date . "'");

        }

        $users = $usersQuery->select('*');

        return datatables()->of($users)

            ->make(true);
            return view('posting.index',$usersQuery);

    }


    function pst($id_posting){
        $data['posting'] =  Posting::with('user')->where('id_posting', $id_posting)->first();
        return view('posting.detail',$data);
    }

     function show(){
      $data['posting'] = Posting::simplePaginate(2);
        return view('posting.index',$data);
     }

     function add(){
      $data['perusahaan'] = Perusahaan::all();
        $data=[
            'action' => url('posting/create'),
            'tombol' =>'Simpan',
            'posting' =>(object)[
               // 'id_perusahaan' => '',
                'nama' =>'',
                'bidang_usaha' =>'',
                 'persyaratan' =>'',
                 'lowongan' =>'',
                 'ttl_p' =>'',
                //  'ttl_tp' =>'',
                 'deskripsi' =>'',
                 'foto' =>'',
                 'lokasi' =>'',
                //  'created_at' =>'',
            ]
            ];
            $data['perusahaan'] = Perusahaan::all();
            return view('posting.from',$data);
     }
   
   // function add(){
   //    $data ['perusahaan'] = Perusahaan::all();
   //    return view('posting.from', $data);
   // }

   //   function create(Request $req){
   //      Posting::create([
   //       'id_admin' => Auth::guard('admin')->user()->id_admin,
   //       'id_perusahaan' => $req->id_perusahaan,
//               'nama' =>$req->nama,
//               'bidang_usaha' =>$req->bidang_usaha,
//               'persyaratan' =>$req->persyaratan,
//               'lowongan' =>$req->lowongan,
//               'ttl_p' =>$req->ttl_p,
//               'ttl_tp' =>$req->ttl_tp,
//               'deskripsi' =>$req->deskripsi,
//               'foto' =>$req->foto,
//               'lokasi' =>$req->lokasi,
   //      ]);
   //      return redirect('posting');
   //   }

   function create(Request $req){

      $foto = $req->file('foto')->store('foto', 'public');

      Posting::create([
         'id_admin' => Auth::guard('admins')->user()->id_admin,
         'id_perusahaan' => $req->id_perusahaan,
         // 'nama' =>$req->nama,
         'bidang_usaha' =>$req->bidang_usaha,
         'persyaratan' =>$req->persyaratan,
         'lowongan' =>$req->lowongan,
         'ttl_p' =>$req->ttl_p,
        //  'ttl_tp' =>2023-12-02,
         'deskripsi' =>$req->deskripsi,
         'foto' =>$foto,
         'lokasi' =>$req->lokasi,
        //  'created_at' =>$req->created_at,
        ]);
        return redirect('posting');
   }

     function hapus($id){
      // $posting = Posting::where('id_posting', $id)->first();
      // $posting->delete();
      // Storage::delete($posting->foto );
        $posting = Posting::where('id_posting',$id)->delete();
        return redirect('posting');
     }

     function edit($id_posting){
        // $data['posting'] = Posting::find($id_posting);
        $data['posting'] = Posting::with('user')->where('id_posting', $id_posting)->first();
        $data['action'] = url('posting/update'). '/' .$data['posting']->id_posting;
        $data['tombol'] = 'Update';

        return view('posting.update',$data);
     }

     function update(Request $req){
        

      $foto = $req->file('foto')->store('foto', 'public');

      if($req->file('foto')){

        $file = $req->file('foto')->store('foto');
    }else{
        $file = DB::raw('foto');
    }

        Posting::where('id_posting',$req->id)->update([
            // 'nama' =>$req->nama,
            'bidang_usaha' =>$req->bidang_usaha,
            'persyaratan' =>$req->persyaratan,
            'lowongan' =>$req->lowongan,
            'ttl_p' =>$req->ttl_p,
            // 'ttl_tp' =>$req->ttl_tp,
            'deskripsi' =>$req->deskripsi,
            'foto' =>$foto,
            'lokasi' =>$req->lokasi,
            'created_at' =>$req->created_at,
        ]);
        return redirect('posting');
     }
}
