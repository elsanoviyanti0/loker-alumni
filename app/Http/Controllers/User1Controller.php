<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class User1Controller extends Controller
{
    //
    function show(){
        return view('footer.daftar');
    }

    function view(){
        $data['user'] = User::all();
        return view('alumni.index',$data);
    }

    public function create(Request $request)
    {
        // Validation
        $request->validate([
            'nisn' => 'required|string',
            'name' => 'required|string',
            'ktp' => 'required|string|min:10',
            'tanggal_lahir' => 'required|date',
            'alamat' => 'required|string',
            'no_hp' => 'required|string',
            'ttn_lulus' => 'required|string',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
        ]);

        // Handle file upload
        $foto = $request->file('foto')->store('foto', 'public');

        // Create User
        $user = User::create([
            'nisn' => $request->nisn,
            'name' => $request->name,
            'ktp' => $request->ktp,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'no_hp' => $request->no_hp,
            'ttn_lulus' => $request->ttn_lulus,
            // "level" => $request->level,
            'foto' => $foto,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        return redirect('login')->with('success', 'Registration successful! Please log in.');
    }

    function userupdate(Request $req, $id){
        User::Where('id', $id)->update([
            'nisn' => $req->nisn,
            'name' => $req->name,
            'ktp' => $req->ktp,
            'tanggal_lahir' => $req->tanggal_lahir,
            'alamat' => $req->alamat,
            'no_hp' => $req->no_hp,
            'ttn_lulus' => $req->ttn_lulus,
            'foto' => $req->foto,
            'email' => $req->email,
            'password' => $req->password
        ]);

        return redirect('myuser');
    }

    function auth(Request $req){
        $credentials = $req->only('email','password');

        // if (Auth::attempt($credentials)) {

        //     if (Auth::user()->level === "member") {
        //         return redirect('stisla');

        //     }else if (Auth::user()->level  === "alumni"){
        //         return redirect('beranda');
        //     }
        
        if(Auth::attempt($credentials)){
            return redirect('landingpage');

        }
        return redirect()->back();
    }

    function logout(){
        Auth::logout();
        return redirect('login');
    }
}
