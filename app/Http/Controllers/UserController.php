<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    //
    function register(){
        return view('footer.register');
    }
    function create(Request $req){
        $validate = $this->validate($req,[
            'name' => 'required|string',
            'email' => 'required|string|min:8',
            'password' => 'required_with:confirm_password|min:8|same:confirm_password',
            'jabatan' => 'required|string',

        ]);
        $validate['password']=bcrypt($req->password);
        User::create($validate);
        return redirect('footer.login_adm');
    }
    function show(){
        $data ['admin'] = User::all();
        return view('layout.dashboard');
    }
    function delete($id){
        User::Where('id',$id)->delete();
        return redirect('layout.dashboard');
    }
}
