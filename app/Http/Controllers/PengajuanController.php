<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengajuan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Posting;

class PengajuanController extends Controller
{
    //
    function show(){
        $data['pengajuan'] = Pengajuan::all();
        return view('pengajuan.index',$data);
    }

    function adm(){
        return view('pengajuan.from');
    }

    function add(){
        $data=[
            'action' =>url('pengajuan/create'),
            'tombol' =>'Simpan',
            'pengajuan' =>(object)[
                // 'nisn' =>'',
                'nama_lengkap' =>'',
                'ttl' =>'',
                'tempat' =>'',
                'alamat' =>'',
                'no_hp' =>'',
                'email' =>'',
                'foto' =>'',
                'dokumen' =>'',
                'ttl_p' =>'',
                // 'status' =>'',

            ]
            ];
            $data['posting'] = Posting::all();
            return view('pengajuan.formldn',$data);
    }

    function create(Request $req){
        // $req->validate([
        //     'nama_lengkap' =>'required|string',
        //     'ttl' =>'required|date',
        //     'tempat' =>'required|string',
        //     'alamat' =>'required|string',
        //     'no_hp' =>'required|number',
        //     'email' => 'required|string|email|unique:users,email',
        //     'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
        //     'dokumen' =>'required|file|mimes:doc,ppt,xls,pdf|max:2048', 
        //     'ttl_p' =>'required|date', 
        
        // ]);

        $foto = $req->file('foto')->store('foto', 'public');

        $dokumen = $req->file('dokumen')->store('dokumen', 'public');

         Pengajuan::create([


          'nisn' =>Auth::guard('web')->user()->nisn,
         'id_posting' => $req->id_posting,
        //  'nisn' => $req->nisn,
            'nama_lengkap' =>$req->nama_lengkap,
            'ttl' =>$req->ttl,
            'tempat' =>$req->tempat,
            'alamat' =>$req->alamat,
            'no_hp' =>$req->no_hp,
            'email' =>$req->email,
            'foto' =>$foto,
            'dokumen' =>$dokumen,
            'ttl_p' =>$req->ttl_p,
            'status' =>0,

        ]);
        return redirect('landingpage');
    }

    function hapus($id){
        // $pengajuan = Pengajuan::where('id_pengajuan', $id)->first();
        // $pengajuan->delete();
        // Storage::delete($pengajuan->foto );
        $pengajuan = Pengajuan::where('id_pengajuan',$id)->delete();
        return redirect('pengajuan');
    }
     
    function edit($id_pengajuan){
        $data['pengajuan'] = Pengajuan::with('user')->where('id_pengajuan', $id_pengajuan)->first();
        $data['action'] = url('pengajuan/update'). '/' .$data['pengajuan']->id_pengajuan;
        $data['tombol'] = 'Update';

        return view('pengajuan.update', $data);
    }
     
    function update(Request $req){
        Pengajuan::where('id',$req->id)->update([
            'nama_lengkap' =>$req->nama_lengkap,
            'ttl' =>$req->ttl,
            'tempat' =>$req->tempat,
            'alamat' =>$req->alamat,
            'no_hp' =>$req->no_hp,
            'email' =>$req->email,
            'foto' =>$req->foto,
            'dokumen' =>$req->dekumen, 
            'ttl_p' =>$req->ttl_p,
            // 'status' =>$req->status,

        ]);

        return redirect('pengajuan');
    }

    function acc($id_pengajuan){

        $data = Pengajuan::find($id_pengajuan);
        $data->status='Aktif';
        $data->save();

        return redirect()->back();
    }

    function rejec($id_pengajuan){

        $data = Pengajuan::find($id_pengajuan);
        $data->status='Tidak Aktif';
        $data->save();

        return redirect()->back();

    }
}
