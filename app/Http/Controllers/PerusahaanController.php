<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Models\Perusahaan;

class PerusahaanController extends Controller
{
    //
    function show(){
        $data['perusahaan'] = Perusahaan::simplePaginate(2);
        return view('Perusahaan.index',$data);
    }

    function add(){
        $data=[
            'action'=>url('perusahaan/create'),
            'tombol'=>'Simpan',
            'perusahaan'=>(object)[
                'nama_perusahaan'=>'',
                'deskripsi'=>'',
                'foto'=>'',
            ]
            ];
            return view('Perusahaan.from',$data);
    }

    function create(Request $req){

        $req->validate([
            'nama_perusahaan' => 'required|string',
            'deskripsi' => 'required|string',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
        ]);

        // Handle file upload
        $foto = $req->file('foto')->store('foto', 'public');

        // Create User
        $perusahaan = Perusahaan::create([
            'nama_perusahaan' => $req->nama_perusahaan,
            'deskripsi' => $req->deskripsi,
            'foto' => $foto,
        ]);

        return redirect('perusahaan')->with('success', 'Berhasil Menambahkan data');

    }

    function hapus($id){
        $perusahaan = Perusahaan::where('id_perusahaan', $id)->first();
        $perusahaan->delete();
        Storage::delete($perusahaan->foto );
        // $perusahaan = Perusahaan::where('id',$id)->delete();
        return redirect('perusahaan');
    }

    function edit($id_perusahaan){
        $data['perusahaan'] = Perusahaan::find($id_perusahaan);
        $data['action'] = url('perusahaan/update'). '/' .$data['perusahaan']->id_perusahaan;
        $data['tombol'] = 'Update';

        return view('Perusahaan.from',$data);
    }

    function update(Request $req){
        $foto = $req->file('foto')->store('foto', 'public');

        $this->validate($req, [
             'nama_perusahaan' => 'required|string',
            'deskripsi' => 'required|string',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
        ]);


        Perusahaan::where('id_perusahaan',$req->id)->update([
            'nama_perusahaan'=>$req->nama_perusahaan,
            'deskripsi'=>$req->deskripsi,
            'foto'=>$foto,
        ]);
        return redirect('perusahaan');
    }
}
