<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Pengajuan;
use App\Models\Perusahaan;
use App\Models\Posting;
use App\Models\User;
use App\Models\Kontak;
use App\Models\Testimoni;
use Illuminate\Support\Carbon;

class TampilController extends Controller
{
    //
    function date(){

        $currentDate = Carbon::now();
        $posts = Posting::where('created_at', '>', $currentDate)->get();
    
        return view('layout.landingpage', compact('posts'));
    }

    function show(){
        $data['perusahaan'] = Perusahaan::all();
        $data['posting'] = Posting::all();
        return view('layout.tivo',$data);
    }

    function us(){
        $data['admin'] = Admin::all();
            return view('layout.profile',$data);
    }
     
    function lading(){
        // $data['user'] = User::all();
        $currentDate = Carbon::now();
        $data['pengajuan'] = Pengajuan::where('status','=','aktif')->get();
        // $data['kontak'] = Kontak::where('status','=','aktif')->get();
        $data['posting'] = Posting::where('created_at', '>', $currentDate)->get();
        $data['testimoni'] = Testimoni::all();
        $data['perusahaan'] = Perusahaan::all();
        return view('layout.landingpage',$data);
    }

    function informasi($id_posting){
        $posting['posting'] = Posting::with('user')->where('id_posting', $id_posting)->first();
        // $data['posting'] = Posting::find($id_posting);
        return view('layout.informasi',$posting);
    }

    function detail($id_pengajuan){
        $data['pengajuan'] = Pengajuan::with('user')->where('id_pengajuan', $id_pengajuan)->first();
        return view('pengajuan.detail',$data);
    }
    function profile($nisn){
        $data['user'] = User::find($nisn);
        return view('alumni.profilAlm',$data);
    }

    function posting(){
        $data['posting'] = Posting::all();
        return view('layout.dashboard',$data);
    }
}
