<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kontak;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class KontakController extends Controller
{
    //
    function show(){
        $data['kontak'] = Kontak::all();
        return view('kontak.index',$data);
    }

    function add(){
        $data['kontak'] = Kontak::all();
            return view('layout.landingpage',$data);
    }

    function create(Request $req){

        $req->validate([
            'nama' => 'required|string',
            'email' => 'required|string',
            'pesan' => 'required|string', 
            // 'status' => 'required|string', 
        ]);

        // Handle file upload
        // $foto = $req->file('foto')->store('foto', 'public');

        // Create User
        $kontak = Kontak::create([
            'nama' => $req->nama,
            'email' => $req->email,
            'pesan' => $req->pesan,
            'status' =>0,
        ]);

        return redirect('kontak')->with('success', 'Berhasil Menambahkan data');

    }

    function hapus($id){
        // $kontak = Kontak::where('id', $id)->first();
        // $kontak->delete();
        // Storage::delete($kontak->foto );
        $kontak = Kontak::where('id',$id)->delete();
        return redirect('kontak');
    }

    function edit($id){
        $data['kontak'] = Kontak::find($id);
        $data['action'] = url('kontak/update'). '/' .$data['kontak']->id;
        $data['tombol'] = 'Update';

        return view('Perusahaan.from',$data);
    }

    function update(Request $req){

        $this->validate($req, [
            'nama' => 'required|string',
            'email' => 'required|string',
            'pesan' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
        ]);


        Kontak::where('id',$req->id)->update([
            'nama'=>$req->nama,
            'email'=>$req->email,
            'pesan'=>$req->pesan,
            // 'status'=>$req->status,
        ]);
        return redirect('perusahaan');
    }

    function acc($id){

        $data = Kontak::find($id);
        $data->status='Aktif';
        $data->save();

        return redirect()->back();
    }

    function rejec($id){

        $data = Kontak::find($id);
        $data->status='Tidak Aktif';
        $data->save();

        return redirect()->back();

    }
}
