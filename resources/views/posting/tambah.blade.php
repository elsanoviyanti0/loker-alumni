<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>General Dashboard &mdash; Stisla</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="adm/assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="adm/assets/modules/fontawesome/css/all.min.css" >

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="adm/assets/modules/jqvmap/dist/jqvmap.min.css">
  <link rel="stylesheet" href="adm/assets/modules/weather-icon/css/weather-icons.min.css">
  <link rel="stylesheet" href="adm/assets/modules/weather-icon/css/weather-icons-wind.min.css">
  <link rel="stylesheet" href="adm/assets/modules/summernote/summernote-bs4.css" >

  <!-- Template CSS -->
  <link rel="stylesheet" href="adm/assets/css/style.css" >
  <link rel="stylesheet" href="adm/assets/css/components.css">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar" style="background: rgb(65, 65, 230)">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
            <div class="search-result">
              <div class="search-item">
                <a href="#">
                  <img class="mr-3 rounded" width="30" src="assets/img/products/product-2-50.png" alt="product">
                  Drone X2 New Gen-7
                </a>
              </div>
              <div class="search-item">
                <a href="#">
                  <img class="mr-3 rounded" width="30" src="assets/img/products/product-1-50.png" alt="product">
                  Headphone Blitz
                </a>
              </div>
              <div class="search-header">
                Projects
              </div>
              <div class="search-item">
                <a href="#">
                  <div class="search-icon bg-danger text-white mr-3">
                    <i class="fas fa-code"></i>
                  </div>
                  Stisla Admin Template
                </a>
              </div>
              <div class="search-item">
                <a href="#">
                  <div class="search-icon bg-primary text-white mr-3">
                    <i class="fas fa-laptop"></i>
                  </div>
                  Create a new Homepage Design
                </a>
              </div>
            </div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="adm/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, Ujang Maman</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="features-activities.html" class="dropdown-item has-icon">
                <i class="fas fa-bolt"></i> Activities
              </a>
              <a href="features-settings.html" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Stisla</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="dropdown active">
              <a href="#"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Pages</li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Alumni</span></a>
              <ul class="dropdown-menu">
                <li><a href="auth-forgot-password.html">Data Alumni</a></li> 
                <li><a href="auth-login.html">Tambah Alumni</a></li> 
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Perusahaan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="layout-default.html">Data Perusahaan</a></li>
                <li><a class="nav-link" href="layout-transparent.html">Tambah Perusahaan</a></li>
              </ul>
            </li>
            <li><a class="nav-link  has-dropdown" data-toggle="dropdown"><i class="far fa-square"></i> <span>Lamaran</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="layout-default.html">Data Lamaran</a></li>
                <li><a class="nav-link" href="layout-transparent.html">Tambah Lamaran</a></li>
              </ul></li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Posting</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="bootstrap-alert.html">Data Posting</a></li>
                <li><a class="nav-link" href="bootstrap-badge.html">Tambah Posting</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Pengajuan</span></a>
              <ul class="dropdown-menu">
                <li><a href="utilities-contact.html">Data Pengajuan</a></li>
                <li><a class="nav-link" href="utilities-invoice.html">Tambah Pengajuan</a></li>
              </ul>
            </li>          
          </ul>

          <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
              <i class="fas fa-rocket"></i> Documentation
            </a>
          </div>        </aside>
        </div>

            <!-- Main Content -->
            <div class="main-content">
              <section class="section">
                <div class="section-header">
                  <h1>Form Posting</h1>
                  <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Forms</a></div>
                    <div class="breadcrumb-item">Form Posting</div>
                  </div>
                </div>
                <div class="section-body">
                  <h2 class="section-title">Form Posting</h2>
                  <p class="section-lead">
                  
                  </p>
                  <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                      <div class="card">
                        <form class="needs-validation" novalidate="">
                          <div class="card-header">
                            <h4>Horizontal Form</h4>
                          </div>
                          {{-- <form action="{{ $action }}" method="post" enctype="multipart/form-data">
                            @csrf --}}
                          <div class="card-body">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Nama</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Nama Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Bidang Usaha</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Bidang Usaha Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Persyaratan</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Persyaratan Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Lowongan</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Lowongan Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Tanggal Posting</label>
                              <div class="col-sm-9">
                                <input type="date" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Tanggal Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Tanggal Berakhir Posting</label>
                              <div class="col-sm-9">
                                <input type="date" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Tanggal Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Deskripsi</label>
                              <div class="col-sm-9">
                                <textarea class="form-control" required=""></textarea>
                                <div class="invalid-feedback">
                                  Deskripsi Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Foto</label>
                              <div class="col-sm-9">
                                <input type="file" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Foto Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Lokasi</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Lokasi Harus Di isi!
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary " style="background: rgb(65, 65, 230)">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
        </div>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Smk Ypc Tasikmalaya  2023 <div class="bullet"></div> Design By <a href="">Elsa Novianti</a>
        </div>
        <div class="footer-right">
          
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="adm/assets/modules/jquery.min.js"></script>
  <script src="adm/assets/modules/popper.js"></script>
  <script src="adm/assets/modules/tooltip.js"></script>
  <script src="adm/assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="adm/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="adm/assets/modules/moment.min.js"></script>
  <script src="adm/assets/js/stisla.js"></script>
  
  <!-- JS Libraies -->
  <script src="adm/assets/modules/simple-weather/jquery.simpleWeather.min.js"></script>
  <script src="adm/assets/modules/chart.min.js"></script>
  <script src="adm/assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
  <script src="adm/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
  <script src="adm/assets/modules/summernote/summernote-bs4.js"></script>
  <script src="adm/assets/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

  <!-- Page Specific JS File -->
  <script src="adm/assets/js/page/index-0.js"></script>
  
  <!-- Template JS File -->
  <script src="adm/assets/js/scripts.js"></script>
  <script src="adm/assets/js/custom.js"></script>
</body>
</html>



            <!-- Main Content -->
            <div class="main-content">
              <section class="section">
                <div class="section-header">
                  <h1>Form Posting</h1>
                  <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Forms</a></div>
                    <div class="breadcrumb-item">Form Posting</div>
                  </div>
                </div>
                <div class="section-body">
                  <h2 class="section-title">Form Posting</h2>
                  <p clas<!DOCTYPE html>
                    <html lang="en">
                    <head>
                      <meta charset="UTF-8">
                      <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
                      <title>General Dashboard &mdash; Stisla</title>
                    
                      <!-- General CSS Files -->
                      <link rel="stylesheet" href="adm/assets/modules/bootstrap/css/bootstrap.min.css">
                      <link rel="stylesheet" href="adm/assets/modules/fontawesome/css/all.min.css" >
                    
                      <!-- CSS Libraries -->
                      <link rel="stylesheet" href="adm/assets/modules/jqvmap/dist/jqvmap.min.css">
                      <link rel="stylesheet" href="adm/assets/modules/weather-icon/css/weather-icons.min.css">
                      <link rel="stylesheet" href="adm/assets/modules/weather-icon/css/weather-icons-wind.min.css">
                      <link rel="stylesheet" href="adm/assets/modules/summernote/summernote-bs4.css" >
                    
                      <!-- Template CSS -->
                      <link rel="stylesheet" href="adm/assets/css/style.css" >
                      <link rel="stylesheet" href="adm/assets/css/components.css">
                    <!-- Start GA -->
                    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
                    <script>
                      window.dataLayer = window.dataLayer || [];
                      function gtag(){dataLayer.push(arguments);}
                      gtag('js', new Date());
                    
                      gtag('config', 'UA-94034622-3');
                    </script>
                    <!-- /END GA --></head>
                    
                    <body>
                      <div id="app">
                        <div class="main-wrapper main-wrapper-1">
                          <div class="navbar-bg"></div>
                          <nav class="navbar navbar-expand-lg main-navbar" style="background: rgb(65, 65, 230)">
                            <form class="form-inline mr-auto">
                              <ul class="navbar-nav mr-3">
                                <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                                <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
                              </ul>
                              <div class="search-element">
                                <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                                <div class="search-backdrop"></div>
                                <div class="search-result">
                                  <div class="search-item">
                                    <a href="#">
                                      <img class="mr-3 rounded" width="30" src="assets/img/products/product-2-50.png" alt="product">
                                      Drone X2 New Gen-7
                                    </a>
                                  </div>
                                  <div class="search-item">
                                    <a href="#">
                                      <img class="mr-3 rounded" width="30" src="assets/img/products/product-1-50.png" alt="product">
                                      Headphone Blitz
                                    </a>
                                  </div>
                                  <div class="search-header">
                                    Projects
                                  </div>
                                  <div class="search-item">
                                    <a href="#">
                                      <div class="search-icon bg-danger text-white mr-3">
                                        <i class="fas fa-code"></i>
                                      </div>
                                      Stisla Admin Template
                                    </a>
                                  </div>
                                  <div class="search-item">
                                    <a href="#">
                                      <div class="search-icon bg-primary text-white mr-3">
                                        <i class="fas fa-laptop"></i>
                                      </div>
                                      Create a new Homepage Design
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </form>
                            <ul class="navbar-nav navbar-right">
                              <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                                <img alt="image" src="adm/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
                                <div class="d-sm-none d-lg-inline-block">Hi, Ujang Maman</div></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                  <div class="dropdown-title">Logged in 5 min ago</div>
                                  <a href="features-profile.html" class="dropdown-item has-icon">
                                    <i class="far fa-user"></i> Profile
                                  </a>
                                  <a href="features-activities.html" class="dropdown-item has-icon">
                                    <i class="fas fa-bolt"></i> Activities
                                  </a>
                                  <a href="features-settings.html" class="dropdown-item has-icon">
                                    <i class="fas fa-cog"></i> Settings
                                  </a>
                                  <div class="dropdown-divider"></div>
                                  <a href="#" class="dropdown-item has-icon text-danger">
                                    <i class="fas fa-sign-out-alt"></i> Logout
                                  </a>
                                </div>
                              </li>
                            </ul>
                          </nav>
                          <div class="main-sidebar sidebar-style-2">
                            <aside id="sidebar-wrapper">
                              <div class="sidebar-brand">
                                <a href="index.html">Stisla</a>
                              </div>
                              <div class="sidebar-brand sidebar-brand-sm">
                                <a href="index.html">St</a>
                              </div>
                              <ul class="sidebar-menu">
                                <li class="menu-header">Dashboard</li>
                                <li class="dropdown active">
                                  <a href="#"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                                </li>
                                <li class="menu-header">Pages</li>
                                <li class="dropdown">
                                  <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Alumni</span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="auth-forgot-password.html">Data Alumni</a></li> 
                                    <li><a href="auth-login.html">Tambah Alumni</a></li> 
                                  </ul>
                                </li>
                                <li class="dropdown">
                                  <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Perusahaan</span></a>
                                  <ul class="dropdown-menu">
                                    <li><a class="nav-link" href="layout-default.html">Data Perusahaan</a></li>
                                    <li><a class="nav-link" href="layout-transparent.html">Tambah Perusahaan</a></li>
                                  </ul>
                                </li>
                                <li><a class="nav-link  has-dropdown" data-toggle="dropdown"><i class="far fa-square"></i> <span>Lamaran</span></a>
                                  <ul class="dropdown-menu">
                                    <li><a class="nav-link" href="layout-default.html">Data Lamaran</a></li>
                                    <li><a class="nav-link" href="layout-transparent.html">Tambah Lamaran</a></li>
                                  </ul></li>
                                <li class="dropdown">
                                  <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Posting</span></a>
                                  <ul class="dropdown-menu">
                                    <li><a class="nav-link" href="bootstrap-alert.html">Data Posting</a></li>
                                    <li><a class="nav-link" href="bootstrap-badge.html">Tambah Posting</a></li>
                                  </ul>
                                </li>
                                <li class="dropdown">
                                  <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Pengajuan</span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="utilities-contact.html">Data Pengajuan</a></li>
                                    <li><a class="nav-link" href="utilities-invoice.html">Tambah Pengajuan</a></li>
                                  </ul>
                                </li>          
                              </ul>
                    
                              <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                                <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
                                  <i class="fas fa-rocket"></i> Documentation
                                </a>
                              </div>        </aside>
                            </div>
                    
                                <!-- Main Content -->
                                {{-- <div class="main-content"> --}}
                                    <section class="section">
                                      <div class="section-header">
                                        <h1>Form Posting</h1>
                                        <div class="section-header-back">
                                        </div>
                                      </div>
                                        <h1>Create Posting</h1>
                                      </div>

                                      <div class="section-body">

                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Buat Posting</h4>
                                            </div>
                                            <div class="card-body">
                                                <form action="" method="post">
                                                    <div class="form-group">
                                                        <label for="">Nama</label>
                                                        <input type="text" name="nama" id="nama" class="form-control" required autofocus>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                      </div>
                                    </section>
                                </div>
                            </div>
                          </div>
                          <footer class="main-footer">
                            <div class="footer-left">
                              Smk Ypc Tasikmalaya  2023 <div class="bullet"></div> Design By <a href="">Elsa Novianti</a>
                            </div>
                            <div class="footer-right">
                              
                            </div>
                          </footer>
                        </div>
                      </div>
                    
                      <!-- General JS Scripts -->
                      <script src="adm/assets/modules/jquery.min.js"></script>
                      <script src="adm/assets/modules/popper.js"></script>
                      <script src="adm/assets/modules/tooltip.js"></script>
                      <script src="adm/assets/modules/bootstrap/js/bootstrap.min.js"></script>
                      <script src="adm/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
                      <script src="adm/assets/modules/moment.min.js"></script>
                      <script src="adm/assets/js/stisla.js"></script>
                      
                      <!-- JS Libraies -->
                      <script src="adm/assets/modules/simple-weather/jquery.simpleWeather.min.js"></script>
                      <script src="adm/assets/modules/chart.min.js"></script>
                      <script src="adm/assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
                      <script src="adm/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
                      <script src="adm/assets/modules/summernote/summernote-bs4.js"></script>
                      <script src="adm/assets/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>
                    
                      <!-- Page Specific JS File -->
                      <script src="adm/assets/js/page/index-0.js"></script>
                      
                      <!-- Template JS File -->
                      <script src="adm/assets/js/scripts.js"></script>
                      <script src="adm/assets/js/custom.js"></script>
                    </body>
                    </html>s="section-lead">
                  
                  </p>
                  <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                      <div class="card">
                        <form class="needs-validation" novalidate="">
                          <div class="card-header">
                            <h4>Horizontal Form</h4>
                          </div>
                          {{-- <form action="{{ $action }}" method="post" enctype="multipart/form-data">
                            @csrf --}}
                          <div class="card-body">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Nama</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Nama Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Bidang Usaha</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Bidang Usaha Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Persyaratan</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Persyaratan Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Lowongan</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Lowongan Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Tanggal Posting</label>
                              <div class="col-sm-9">
                                <input type="date" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Tanggal Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Tanggal Berakhir Posting</label>
                              <div class="col-sm-9">
                                <input type="date" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Tanggal Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Deskripsi</label>
                              <div class="col-sm-9">
                                <textarea class="form-control" required=""></textarea>
                                <div class="invalid-feedback">
                                  Deskripsi Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Foto</label>
                              <div class="col-sm-9">
                                <input type="file" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Foto Harus Di isi!
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Lokasi</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" required="">
                                <div class="invalid-feedback">
                                  Lokasi Harus Di isi!
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary " style="background: rgb(65, 65, 230)">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
           