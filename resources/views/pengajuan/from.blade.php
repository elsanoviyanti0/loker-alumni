<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Dashboard</title>

  <!-- Favicons -->
<link href="{{ asset('img/logo smk.png')}}" rel="icon">
<link href="{{ asset('img/logo smk.png')}}" rel="apple-touch-icon">
<link rel="shortcut icon" href="img/logo smk.png" type="image/x-icon">

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/jqvmap/dist/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons-wind.min.css')}}">
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/summernote/summernote-bs4.css')}}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('adm/assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/css/components.css')}}">
  <link rel="shortcut icon" href="{{asset('img/logo smk.png" type="image/x-icon')}}">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar" style="background: rgb(65, 65, 230)">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
            <div class="search-result">
            </div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="adm/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, Ujang Maman</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="http://127.0.0.1:8000/tampilan/profile" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <div class="dropdown-divider"></div>
              <a href="http://127.0.0.1:8000/hm" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href=""></a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href=""></a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="dropdown active">
              <a href="http://localhost:8000/stisla"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Pages</li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Admin</span></a>
              <ul class="dropdown-menu">
                <li><a href="http://localhost:8000/admin1">Data Admin</a></li> 
              </ul>
            </li>
            <li><a class="nav-link  has-dropdown" data-toggle="dropdown"><i class="far fa-square"></i> <span>Alumni</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="http://localhost:8000/alumni">Data Alumni</a></li>
                  {{-- <li><a class="nav-link" href="layout-transparent.html">Tambah Lamaran</a></li> --}}
                </ul></li>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Perusahaan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://localhost:8000/perusahaan">Data Perusahaan</a></li>
                {{-- <li><a class="nav-link" href="layout-transparent.html">Tambah Perusahaan</a></li> --}}
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Posting</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://localhost:8000/posting">Data Posting</a></li>
                {{-- <li><a class="nav-link" href="bootstrap-badge.html">Tambah Posting</a></li> --}}
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Pengajuan</span></a>
              <ul class="dropdown-menu">
                <li><a href="http://localhost:8000/pengajuan">Data Pengajuan</a></li>
                {{-- <li><a class="nav-link" href="utilities-invoice.html">Tambah Pengajuan</a></li> --}}
              </ul>
            </li>          
          </ul>
        </div>

      <!-- Main Content -->
   <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Form Pengajuan</h1>
        {{-- <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Forms</a></div>
          <div class="breadcrumb-item">Form Posting</div>
        </div> --}}
      </div>
      <div class="section-body">
        <h2 class="section-title">Form Pengajuan</h2>
        <p class="section-lead">
        </p>
        <div class="row">
          <div class="col-12 col-md-6 col-lg-6">
            <div class="card">
              {{-- <form class="needs-validation" novalidate=""> --}}
                <div class="card-header">
                  <h4>Horizontal Form</h4>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <form action="{{ $action }}" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="card-body">
                      <div class="form-group row">
                        {{-- <div class="form-group"> --}}
                          <label class="col-sm-3 col-form-label">Lowongan</label>
                          <div class="col-sm-9">
                            <select name="id_posting" id="id_posting" class="form-control">
                              <option value="">Pilih Lowongan</option>
                              @foreach ($posting as $item)
                              <option value="{{$item->id_posting}}">{{$item->lowongan}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                        <input type="text" name="nama_lengkap" class="form-control" required="" value="{{$pengajuan->nama_lengkap}}">
                        <div class="invalid-feedback">
                            Nama Lengkap Harus Di isi!
                        </div>
                        </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                      <div class="col-sm-9">
                      <input type="date" name="ttl" class="form-control" required="" value="{{$pengajuan->ttl}}">
                      <div class="invalid-feedback">
                          Tanggal Lahir Harus Di isi!
                      </div>
                      </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Tempat</label>
                    <div class="col-sm-9">
                    <input type="text" name="tempat" class="form-control" required="" value="{{$pengajuan->tempat}}">
                    <div class="invalid-feedback">
                        Tempat Harus Di isi!
                    </div>
                    </div>
                </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                        <textarea class="form-control" name="alamat" required="" value="{{$pengajuan->alamat}}"></textarea>
                        <div class="invalid-feedback">
                            Alamat Harus Di isi!
                        </div>
                        </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">No Hp</label>
                      <div class="col-sm-9">
                      <input type="number" name="no_hp" class="form-control" required="" value="{{$pengajuan->no_hp}}">
                      <div class="invalid-feedback">
                          No Hp Harus Di isi!
                      </div>
                      </div>
                   </div>
                   <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9">
                    <input type="email" name="email" class="form-control" required="" value="{{$pengajuan->email}}">
                    <div class="invalid-feedback">
                        Email Harus Di isi!
                    </div>
                    </div>
                 </div>
                 <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Foto</label>
                        <div class="col-sm-9">
                        <input type="file" name="foto" id="foto" class="form-control" required="" accept="image/*">
                        <div class="invalid-feedback">
                            Foto Harus Di isi!
                        </div>
                        </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Dokument</label>
                      <div class="col-sm-9">
                      <input type="file" name="dokumen" class="form-control" accept="pdf/*" required="" value="{{$pengajuan->dokumen}}">
                      <div class="invalid-feedback">
                          Dokumen Harus Di isi!
                      </div>
                      </div>
                   </div>
                   <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Tanggal Pengajuan</label>
                    <div class="col-sm-9">
                    <input type="date" name="ttl_p" class="form-control" required="" value="{{$pengajuan->ttl_p}}">
                    <div class="invalid-feedback">
                        Tanggal Pengajuan Harus Di isi!
                    </div>
                    </div>
                 </div>
                 <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Status</label>
                  <div class="col-sm-9">
                  <input type="text" name="status" class="form-control"  required="" value="{{$pengajuan->status}}">
                  <div class="invalid-feedback">
                      Status Harus Di isi!
                  </div>
                  </div>
               </div>
                    <div class="card-footer text-right">
                    <button class="btn btn-primary " style="background: rgb(65, 65, 230)">Submit</button>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Smk Ypc Tasikmalaya  2023 <div class="bullet"></div> Design By <a href="">Elsa Novianti</a>
        </div>
        <div class="footer-right">
          
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('adm/assets/modules/jquery.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/popper.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/tooltip.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('adm/assets/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/moment.min.js')}}"></script>
  <script src="{{ asset('adm/assets/js/stisla.js')}}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('adm/assets/modules/simple-weather/jquery.simpleWeather.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chart.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/jquery.vmap.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/summernote/summernote-bs4.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>

  <!-- Page Specific JS File -->
  <script src="{{ asset('adm/assets/js/page/index-0.js')}}"></script>
  
  <!-- Template JS File -->
  <script src="{{ asset('adm/assets/js/scripts.js')}}"></script>
  <script src="{{ asset('adm/assets/js/custom.js')}}"></script>

</body>
</html>