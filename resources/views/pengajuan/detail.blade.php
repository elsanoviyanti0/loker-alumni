
<link href="{{ asset('img/logo smk.png')}}" rel="icon">
<link href="{{ asset('img/logo smk.png')}}" rel="apple-touch-icon">
<link rel="shortcut icon" href="img/logo smk.png" type="image/x-icon">

<link rel="stylesheet" href="{{ asset('adm/assets/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/jqvmap/dist/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons-wind.min.css')}}">
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/summernote/summernote-bs4.css')}}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('adm/assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/css/components.css')}}">
  <link rel="shortcut icon" href="{{asset('img/logo smk.png" type="image/x-icon')}}">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
    <div id="app">
    <div class="main-wrapper main-wrapper-1">
      {{-- <div class="navbar-bg"></div> --}}
      {{-- <nav class="navbar navbar-expand-lg main-navbar" style="background: rgb(65, 65, 230)"> --}}
          <form class="form-inline mr-auto">
    </form>
      
      </nav>
     
      
      
      <!-- Main Content -->
      <div class="main">
          <section class="section">
          <div class="section-header">
            <h1>Informasi</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Posting</div>
            </div>
        </div>
          <div class="section-body">
              {{-- @foreach ($admin as $item) --}}
                
              <h2 class="section-title">Hi, {{ $pengajuan->nama_lengkap }}!</h2>
            <p class="section-lead">
              {{-- Change information about yourself on this page. --}}
            </p>
            
            <div class="row mt-sm-2">
              <div class="col-12 col-md-6 col-lg-5">
                <div class="card profile-widget">
                    <div class="profile-widget-header">                     
                        <img alt="image" src="/storage/{{ $pengajuan->foto }}" class="rounded-circle profile-widget-picture">
                        <div class="profile-widget-items">
                    </div>
                </div>
                <div class="profile-widget-description">
                  <div class="profile-widget-name">{{ $pengajuan->nama_lengkap }} <div class="text-muted d-inline font-weight-normal"><div class="slash"></div>{{ $pengajuan->alamat }}</div></div>
                  {{-- {{ $posting->deskripsi }}  <b>Indonesia</b>, especially in my family. He is not a fictional character but an original hero in my family, a hero for his children and for his wife. So, I use the name as a user in this template. Not a tribute, I'm just bored with <b>'John Doe'</b>. --}}
                    {{-- {{ $posting->deskripsi }}  --}}
                  </div>
                  <div class="card-footer text-center">
                    {{-- <div class="font-weight-bold mb-2">Follow Ujang On</div>
                    <a href="#" class="btn btn-social-icon btn-facebook mr-1">
                      <i class="fab fa-facebook-f"></i>
                    </a> --}}
                    <a href="#" class="btn btn-social-icon btn-twitter mr-1">
                      <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-github mr-1">
                      <i class="fab fa-github"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-instagram">
                      <i class="fab fa-instagram"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-7">
                <div class="card">
                  <form method="post" class="needs-validation" novalidate="">
                    <div class="card-header">
                      <h4>Lowongan</h4>
                    </div>
                    <div class="card-body">
                          
                      <div class="row">    
                        <div class="form-group col-md-6 col-12">
                          <label>Nama Lengkap</label>
                          <input type="text" class="form-control" value=" {{ $pengajuan->nama_lengkap }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>                           
                        <div class="form-group col-md-6 col-12">
                          <label>Tanggal Lahir</label>
                          <input type="text" class="form-control" value=" {{ $pengajuan->ttl }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Tempat</label>
                          <input type="text" class="form-control" value=" {{ $pengajuan->tempat }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Alamat</label>
                          <input type="text" class="form-control" value=" {{ $pengajuan->alamat }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                            <label>No Hp</label>
                            <input type="text" class="form-control" value=" {{ $pengajuan->no_hp }}">
                            <div class="invalid-feedback">
                            </div>
                        </div>
                      <div class="form-group col-md-6 col-12">
                        <label>Email</label>
                        <input type="text" class="form-control" value=" {{ $pengajuan->email }}">
                        <div class="invalid-feedback">
                        </div>
                      </div>
                     <div class="form-group col-md-6 col-12">
                        <label>Tanggal Pengajuan</label>
                        <input type="text" class="form-control" value=" {{ $pengajuan->ttl_p }}">
                        <div class="invalid-feedback">
                        </div>
                      </div>
                        <div class="row">
                          <div class="form-group mb-0 col-12">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" name="remember" class="custom-control-input" id="newsletter">
                              {{-- <label class="custom-control-label" for="newsletter">Subscribe to newsletter</label> --}}
                              <div class="text-muted form-text">
                                {{-- You will get new information about products, offers and promotions --}}
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                      {{-- <a class="" href="/testimoni/add/{{ $item->id_pengajuan }}"><button class="btn btn-primary" style="background: blue">Tambah Testimoni</button></a> --}}
                        @csrf
                        <a href="http://127.0.0.1:8000/landingpage" class="btn btn-danger">Back</a>
                      {{-- <button class="btn btn-primary">Back</button> --}}
                      {{-- <button class="btn btn-primary">Riwayat</button> --}}
                    </div>
                    {{-- <form action="{{ url('/testimoni/create', $pengajuan->id_pengajuan) }}" method="post">
                      @csrf
                        <div class="card-body">
                          <div class="form-group row">
                           
                              <label class="col-sm-3 col-form-label">Testimoni</label>
                              <input type="hidden" name="id_pengajuan" id="id_pengajuan" value="{{ $pengajuan->id_pengajuan }}">
                            <div class="form-group row">
                              <label></label>
                              <input type="text" class="form-control">
                            <div class="invalid-feedback">
                                Testimoni Harus Di isi!
                            </div>
                        </div>
                        <div class="card-footer text-right">
                        <button class="btn btn-primary " style="background: rgb(65, 65, 230)">Kirim</button>
                        </div>
                  </form> --}}
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Smk Ypc Tasikmalaya  2023 <div class="bullet"></div> Design By <a href="">Elsa Novianti</a>
        </div>
        <div class="footer-right">
          
        </div>
      </footer>
    </div>
  </div>

  <script src="{{ asset('adm/assets/modules/jquery.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/popper.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/tooltip.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('adm/assets/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/moment.min.js')}}"></script>
  <script src="{{ asset('adm/assets/js/stisla.js')}}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('adm/assets/modules/simple-weather/jquery.simpleWeather.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chart.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/jquery.vmap.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/summernote/summernote-bs4.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>
  
  <!-- Page Specific JS File -->
  <script src="{{ asset('adm/assets/js/page/index-0.js')}}"></script>
  
  <!-- Template JS File -->
  <script src="{{ asset('adm/assets/js/scripts.js')}}"></script>
  <script src="{{ asset('adm/assets/js/custom.js')}}"></script>
  
{{-- </body>
</html> --}}