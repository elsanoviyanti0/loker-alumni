Cara filter data
Install Yajra Datatable Package in Laravel

composer require yajra/laravel-datatables-oracle

After successfully Install Yajra Datatable Packages, open config/app.php file and add service provider and alias.
config/app.php

 'providers' => [
   
   Yajra\Datatables\DatatablesServiceProvider::class,
],

 'aliases' => [

  'Datatables' => Yajra\Datatables\Facades\Datatables::class,
] 

After set providers and aliases then publish vendor run by following command.

php artisan vendor:publish

Add Fake Records
We need to add some records in database. Use the below command for add fake records in your database.

php artisan tinker
After run the php artisan tinker. Use the below command. This command will add 150 fake records in your database

>>> factory(App\User::class, 150)->create();

Create Route, Controller & Blade View
Add Route
Now we will add routes in web.php file as like below.

Open routes/web.php file

Route::get('users', 'UsersController@index');

Route::get('users-list', 'UsersController@usersList'); 

Create Controller
We need to create new controller UsersController that will manage two method. lets use this below command and create Controller.

php artisan make:controller UsersController

Now open the controller let’s go to the => app/Http/Controllers/UsersController.php. Put the below Code

1
<?php
2
 
3
namespace App\Http\Controllers;
4
 
5
use Illuminate\Http\Request;
6
use Redirect,Response,DB,Config;
7
use Datatables;
8
use App\User;
9
class UsersController extends Controller
10
{
11
    public function index()
12
    {
13
        return view('users');
14
    }
15
    public function usersList()
16
    {  
17
        $usersQuery = User::query();
18
 
19
        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
20
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
21
 
22
        if($start_date && $end_date){
23
     
24
         $start_date = date('Y-m-d', strtotime($start_date));
25
         $end_date = date('Y-m-d', strtotime($end_date));
26
          
27
         $usersQuery->whereRaw("date(users.created_at) >= '" . $start_date . "' AND date(users.created_at) <= '" . $end_date . "'");
28
        }
29
        $users = $usersQuery->select('*');
30
        return datatables()->of($users)
31
            ->make(true);
32
    }
33
}

Create Blade View
Next, create users.blade.php file in resources/views/ folder and copy past following code.

In this blade view file we will add custom date filter on datatables.

1
<!DOCTYPE html>
2
 
3
<html lang="en">
4
<head>
5
<meta name="csrf-token" content="{{ csrf_token() }}">
6
<title>Laravel DataTable With Custom Filter - Tuts Make</title>
7
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> 
8
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
9
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
10
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
11
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
12
</head>
13
<body>
14
 <div class="container">
15
   <h2>Laravel DataTable With Custom Filter - Tuts Make</h2>
16
   <br>
17
   <div class="row">

    <div class="form-group col-md-6">

    <h5>Start Date <span class="text-danger"></span></h5>

    <div class="controls">

        <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date"> <div class="help-block"></div></div>

    </div>

    <div class="form-group col-md-6">

    <h5>End Date <span class="text-danger"></span></h5>

    <div class="controls">

        <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date"> <div class="help-block"></div></div>

    </div>

    <div class="text-left" style="

    margin-left: 15px;
30
    ">
31
    <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Submit</button>
32
    </div>
33
    </div>
34
    <br>
35
    <table class="table table-bordered" id="laravel_datatable">
36
       <thead>
37
          <tr>
38
             <th>Id</th>
39
             <th>Name</th>
40
             <th>Email</th>
41
             <th>Created at</th>
42
          </tr>
43
       </thead>
44
    </table>
45
 </div>
46
 <script>
47
 $(document).ready( function () {
48
     $.ajaxSetup({
49
          headers: {
50
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
51
          }
52
      });
53
  $('#laravel_datatable').DataTable({
54
         processing: true,
55
         serverSide: true,
56
         ajax: {
57
          url: "{{ url('users-list') }}",
58
          type: 'GET',
59
          data: function (d) {
60
          d.start_date = $('#start_date').val();
61
          d.end_date = $('#end_date').val();
62
          }
63
         },
64
         columns: [
65
                  { data: 'id', name: 'id' },
66
                  { data: 'name', name: 'name' },
67
                  { data: 'email', name: 'email' },
68
                  { data: 'created_at', name: 'created_at' }
69
               ]
70
      });
71
   });
72
 
73
  $('#btnFiterSubmitSearch').click(function(){
74
     $('#laravel_datatable').DataTable().draw(true);
75
  });
76
</script>
77
</body>
78
</html>

Start Development Server
In this step, we will use the php artisan serve command . It will start your server locally

php artisan serve

If you want to run the project diffrent port so use this below command 

php artisan serve --port=8080  
Now we are ready to run our example so run bellow command to quick run.

http://127.0.0.1:8000/users  
If you are not run php artisan server command, direct go to your browser hit below url 
http://localhost/LaravelYajra/public/users

https://www.tutsmake.com/laravel-datatables-custom-filter-and-search-example/