<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Landing Page</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('img/logo smk.png')}}" rel="icon">
  <link href="{{ asset('img/logo smk.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('app/assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  <!-- Favicons -->
<link href="{{ asset('img/logo smk.png')}}" rel="icon">
<link href="{{ asset('img/logo smk.png')}}" rel="apple-touch-icon">
<link rel="shortcut icon" href="img/logo smk.png" type="image/x-icon">

  {{-- Tivo css --}}
  <link href="{{ asset('tivo/cqss/bootstrap.css')}}" rel="stylesheet">
  <link href="{{asset('tivo/css/fontawesome-all.css')}}" rel="stylesheet">
  <link href="{{ asset('tivo/css/swiper.css')}}" rel="stylesheet">
  <link href="{{ asset('tivo/css/magnific-popup.css')}}" rel="stylesheet">
  <link href="{{ asset('tivo/css/styles.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('app/assets/css/style.css')}}" rel="stylesheet">

   <!-- General CSS Files -->
   <link rel="stylesheet" href="{{ asset('adm/assets/modules/bootstrap/css/bootstrap.min.css')}}">
   <link rel="stylesheet" href="{{asset('adm/assets/modules/fontawesome/css/all.min.css')}}">
 
   <!-- CSS Libraries -->
   <link rel="stylesheet" href="{{ asset('adm/assets/modules/jqvmap/dist/jqvmap.min.css')}}">
   <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons.min.css')}}">
   <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons-wind.min.css')}}">
   <link rel="stylesheet" href="{{ asset('adm/assets/modules/summernote/summernote-bs4.css')}}">
 
   <!-- Template CSS -->
   <link rel="stylesheet" href="{{asset('adm/assets/css/style.css')}}">
   <link rel="stylesheet" href="{{asset('adm/assets/css/components.css')}}">
   <link rel="shortcut icon" href="{{asset('img/logo smk.png" type="image/x-icon')}}">

  <!-- =======================================================
  * Template Name: Appland
  * Updated: Sep 25 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/free-bootstrap-app-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  
     <!-- Main Content -->
     <div class="main-content">
        <section class="section">
          <div class="section">
            <h1>Form Testimoni</h1>
          </div>
            <div class="row">
              <div class="col-12 col-md-8 col-lg-10">
                <div class="card">
                  {{-- <form class="needs-validation" novalidate=""> --}}
                    <div class="card-header">
                      {{-- <h4>Horizontal Form</h4> --}}
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    {{-- <form action="{{ $action }}" method="post" enctype="multipart/form-data"> --}}
                    <form action="{{ url('/testimoni/create', $pengajuan->id_pengajuan) }}" method="post">
                      @csrf
                        <div class="card-body">
                          <div class="form-group row">
                            {{-- <div class="form-group"> --}}
                              <label class="col-sm-3 col-form-label"></label>
                              <input type="hidden" name="id_pengajuan" id="id_pengajuan" value="{{ $pengajuan->id_pengajuan }}">
                            <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Testimoni</label>
                            <div class="col-sm-9">
                            <input type="text" name="testimoni" id="testimoni" class="form-control" required="">
                            <div class="invalid-feedback">
                                Testimoni Harus Di isi!
                            </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                        <button class="btn btn-primary " style="background: rgb(65, 65, 230)">Kirim</button>
                        </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
   <!-- Vendor JS Files -->
   <script src="{{ asset('app/assets/vendor/aos/aos.js')}}"></script>
   <script src="{{ asset('app/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
   <script src="{{ asset('app/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>
   <script src="{{ asset('app/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
   <script src="{{ asset('app/assets/vendor/php-email-form/validate.js')}}"></script>
 
   <!-- Template Main JS File -->
   <script src="{{ asset('app/assets/js/main.js')}}"></script>
 
    <!-- General JS Scripts -->
  <script src="{{ asset('adm/assets/modules/jquery.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/popper.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/tooltip.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('adm/assets/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/moment.min.js')}}"></script>
  <script src="{{ asset('adm/assets/js/stisla.js')}}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('adm/assets/modules/simple-weather/jquery.simpleWeather.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chart.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/jquery.vmap.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/summernote/summernote-bs4.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>

  <!-- Page Specific JS File -->
  <script src="{{ asset('adm/assets/js/page/index-0.js')}}"></script>
 </body>
 </html>