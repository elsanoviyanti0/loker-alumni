<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Dashboard</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/jqvmap/dist/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons-wind.min.css')}}">
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/summernote/summernote-bs4.css')}}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('adm/assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/css/components.css')}}">
  <link rel="shortcut icon" href="{{asset('img/logo smk.png" type="image/x-icon')}}">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar" style="background: rgb(65, 65, 230)">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
            <div class="search-result">
            </div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="adm/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, Ujang Maman</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <div class="dropdown-divider"></div>
              <a href="logout" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Stisla</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
          </div>
          <ul class="sidebar-menu">
            {{-- <li class="menu-header">Dashboard</li>
            <li class="dropdown active">
              <a href="#"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li> --}}
            <li class="menu-header">Pages</li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Alumni</span></a>
              <ul class="dropdown-menu">
                <li><a href="alumni">Data Alumni</a></li> 
                {{-- <li><a href="auth-login.html">Tambah Alumni</a></li>  --}}
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Perusahaan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="perusahaan">Data Perusahaan</a></li>
                {{-- <li><a class="nav-link" href="layout-transparent.html">Tambah Perusahaan</a></li> --}}
              </ul>
            </li>
            <li><a class="nav-link  has-dropdown" data-toggle="dropdown"><i class="far fa-square"></i> <span>Lamaran</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="lamaran">Data Lamaran</a></li>
                {{-- <li><a class="nav-link" href="layout-transparent.html">Tambah Lamaran</a></li> --}}
              </ul></li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Posting</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="posting">Data Posting</a></li>
                {{-- <li><a class="nav-link" href="bootstrap-badge.html">Tambah Posting</a></li> --}}
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Pengajuan</span></a>
              <ul class="dropdown-menu">
                <li><a href="pengajuan">Data Pengajuan</a></li>
                {{-- <li><a class="nav-link" href="utilities-invoice.html">Tambah Pengajuan</a></li> --}}
              </ul>
            </li>          
          </ul>
        </div>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Smk Ypc Tasikmalaya  2023 <div class="bullet"></div> Design By <a href="">Elsa Novianti</a>
        </div>
        <div class="footer-right">
          
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('adm/assets/modules/jquery.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/popper.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/tooltip.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('adm/assets/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/moment.min.js')}}"></script>
  <script src="{{ asset('adm/assets/js/stisla.js')}}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('adm/assets/modules/simple-weather/jquery.simpleWeather.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chart.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/jquery.vmap.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/summernote/summernote-bs4.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>

  <!-- Page Specific JS File -->
  <script src="{{ asset('adm/assets/js/page/index-0.js')}}"></script>
  
  <!-- Template JS File -->
  <script src="{{ asset('adm/assets/js/scripts.js')}}"></script>
  <script src="{{ asset('adm/assets/js/custom.js')}}"></script>

  <body>
    @yield('content')
</body>
</html>