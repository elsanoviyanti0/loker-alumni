<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Daftar Akun</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../../lgn/assets/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../lgn/assets/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="../../lgn/assets/css/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="../../lgn/assets/images/logo smk.png" data="{{ csrf_token() }}" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth">
        <div class="row flex-grow">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left p-5">
              <div class="brand-logo">
                <img src="../../lgn/assets/images/logo smk.png">
              </div>
              <h3>SMK YPC TASIKMALAYA</h3>
              <h6 class="font-weight-light"></h6>

              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

              <form class="pt-3" action="/daftar/create" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <input type="number" class="form-control form-control-lg" id="exampleInputnisn" placeholder="Nisn"
                    name="nisn">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" id="exampleInputname1" placeholder="Nama"
                    name="name">
                </div>
                <div class="form-group">
                  <input type="number" class="form-control form-control-lg" id="exampleInputktp" placeholder="Ktp"
                    name="ktp">
                </div>
                <div class="form-group">
                  <input type="date" class="form-control form-control-lg" id="exampleInputtanggal_lahir"
                    placeholder="Tanggal Lahir" name="tanggal_lahir">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" id="exampleInputalamat" placeholder="Alamat"
                    name="alamat">
                </div>
                <div class="form-group">
                  <input type="number" class="form-control form-control-lg" id="exampleInputno_hp" placeholder="No hp"
                    name="no_hp">
                </div>
                <div class="form-group">
                  <input type="date" class="form-control form-control-lg" id="exampleInputtahun_lulus"
                    placeholder="Tahun Lulus" name="ttn_lulus">
                </div>
                <div class="form-group">
                  <input type="file" name="foto" id="foto" class="form-control form-control-lg" accept="image/*">
                </div>
                {{-- <div class="form-group">
                  <select class="form-control form-control-lg" id="exampleFormControlSelect2" name="level">
                    <option value="">--Pilih Level--</option>
                    <option value="alumni">Alumni</option>
                    <option value="member">Member</option>
                  </select>
                </div> --}}
                <div class="form-group">
                  <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email"
                    name="email">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" id="exampleInputPassword1"
                    placeholder="Password" name="password">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" id="exampleInputPassword1"
                    placeholder="Confirm Password" name="password_confirmation">
                </div>
                <div class="mt-3">
                  <button type="submit" class="btn btn-primary" style="background: rgb(38, 51, 191)">SIGN UP</button>
                </div>
                <div class="text-center mt-4 font-weight-light">Already have an account? <a href="login" class="text-primary">Login</a>
              </div>
              </div>
              </form>

            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="../../lgn/assets/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="../../lgn/assets/js/off-canvas.js"></script>
  <script src="../../lgn/assets/js/hoverable-collapse.js"></script>
  <script src="../../lgn/assets/js/misc.js"></script>
  <!-- endinject -->
</body>

</html>

