@extends('auth')

@section('title', 'Login-Admin')

@section('content')



<section>
    <div class="form-box">
        <div class="form-value">
              <!-- <img src="img/chat.png" alt="100"  width="100" height="100"> -->
                <div class="login-group">
                    <div class="login-count">
                        {{-- <img src="img/logo smk.png" alt="100"  width="50" height="50"> --}}
                        <h2>LOGIN</h2></div>             
                        <form method="post" action="{{url('login/admin')}}">
                          @csrf
                                <div class="inputbox">
                                    <ion-icon name="mail-outline"></ion-icon>
                                    <input type="email" name="email" required>
                                    <label for="email">Email</label>
                                </div>
                                <div class="inputbox">
                                    <ion-icon name="lock-closed-outline"></ion-icon>
                                    <input type="password" name="password" required>
                                    <label for="password">Password</label>
                                </div>
                                <button>Log In</button>
                        </form>
                            <div class="register">
                                <p>Don't have a account</p><a href="daftar">Register</a>
                            </div>
        </div>
    </div>
</section>
<script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    
@endsection