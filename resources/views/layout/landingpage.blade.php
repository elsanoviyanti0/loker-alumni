<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Landing Page</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('img/logo smk.png')}}" rel="icon">
  <link href="{{ asset('img/logo smk.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('app/assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{ asset('app/assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  {{-- Tivo css --}}
  <link href="{{ asset('tivo/cqss/bootstrap.css')}}" rel="stylesheet">
  <link href="{{asset('tivo/css/fontawesome-all.css')}}" rel="stylesheet">
  <link href="{{ asset('tivo/css/swiper.css')}}" rel="stylesheet">
  <link href="{{ asset('tivo/css/magnific-popup.css')}}" rel="stylesheet">
  <link href="{{ asset('tivo/css/styles.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('app/assets/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Appland
  * Updated: Sep 25 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/free-bootstrap-app-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

   <!-- ======= Header ======= -->
   <header id="header" class="fixed-top  header-transparent ">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1><a href="index.html">BKK SMK YPC</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#ttg_kmi">Tentang Kami</a></li>
          <li><a class="nav-link scrollto" href="#perusahaan">Perusahaan</a></li>
          <li><a class="nav-link scrollto" href="#informasi">Informasi</a></li>
          <li><a class="nav-link scrollto" href="#lowongan">Lowongan</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="getstarted scrollto" href="hm">Log out</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1" data-aos="fade-up">
          <div>
            <h1>Selamat Datang Di</h1>
            <h3 class="py-2">BKK SMK YPC Tasikmalaya</h3>
            <h6>Sebagai wadah dalam mempertemukan tamatan dengan pencari kerja, Memberikan layanan kepada tamatan sesuai dengan tugas dan fungsi masing-masing saksi yang ada dalam BKK, Sebagai wadah dalam pelatihan tamatan yang sesuai dengan permintaan pencari kerja, Sebagai wadah menanam jiwa wirausaha bagi tamatan melalui pelatihan.</h6>
            {{-- <a href="#" class="download-btn"><i class="bx bxl-play-store"></i> Google Play</a>
            <a href="#" class="download-btn"><i class="bx bxl-apple"></i> App Store</a> --}}
          </div>
        </div>
        <div class="col-lg-6 d-lg-flex flex-lg-column align-items-stretch order-1 order-lg-2 hero-img" data-aos="fade-up">
          <img src="assets/img/hero-img.png" class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

      <!-- ======= App Features Section ======= -->
      <section id="ttg_kmi" class="features">
        <div class="container">
  
          <div class="section-title">
            <h2>TENTANG KAMI</h2>
            <P>Bursa Kerja Khusus(BKK) adalah sebuah lembaga yang dibentuk di Sekolah Menengah Kejuruan Negri dan Swasta, Sebagai unit pelaksana yang memberikan pelayanan dan informasi lowongan kerja, pelaksana pemasaran, penyaluran dan penempatan tenaga kerja, merupakan mitra Dinas Tenaga Kerja dan Transmigrasi</P>
            {{-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> --}}
          </div>
  
          <div class="row no-gutters">
            <div class="col-xl-7 d-flex align-items-stretch order-2 order-lg-1">
              <div class="content d-flex flex-column justify-content-center">
                <div class="row">
                  <div class="col-md-6 icon-box" data-aos="fade-up">
                    <i class="bx bx-receipt"></i>
                    {{-- <h4>Corporis voluptates sit</h4> --}}
                    <p>Sebagai wadah dalam mempertemukan tamatan dengan pencari kerja</p>
                  </div>
                  <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                    <i class="bx bx-cube-alt"></i>
                    {{-- <h4>Ullamco laboris nisi</h4> --}}
                    <p>Memberikan layanan kepada tamatan sesuai dengan tugas dan fungsi masing-masing saksi yang ada dalam BKK</p>
                  </div>
                  <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                    <i class="bx bx-images"></i>
                    {{-- <h4>Labore consequatur</h4> --}}
                    <p>tamatan Sebagai wadah dalam pelatihan tamatan yang sesuai dengan permintaan pencari kerja </p> 
                  </div>
                  <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                    <i class="bx bx-shield"></i>
                    {{-- <h4>Beatae veritatis</h4> --}}
                    <p>Sebagai wadah untuk menanamkan jiwa wirausaha bagi tamatan melalui pelatihan</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="image col-xl-5 d-flex align-items-stretch justify-content-center order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
              <img src="app/assets/img/features.svg" class="img-fluid" alt="">
            </div>
          </div>
  
        </div>
      </section><!-- End App Features Section -->

    <!-- ======= Gallery Section ======= -->
    <section id="perusahaan" class="gallery section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>PERUSAHAAN</h2>
          <p>List Perusahaan yang bekerja sama sama dengan pihak BKK SMK YPC Tasikmalaya</p>
        </div>
      </div>

      <div class="container-fluid" data-aos="fade-up">
        <div class="gallery-slider swiper">
          <div class="swiper-wrapper">
            @foreach ($perusahaan as $item)
            <div class="swiper-slide"><a href="/storage/{{ $item->foto }}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="/storage/{{ $item->foto }}" class="img-fluid" alt=""></a></div>
            @endforeach
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Gallery Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials ">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Testimonials</h2>
          {{-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> --}}
        </div>

        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">
            @foreach ($testimoni as $item )
                
            <div class="swiper-slide">
              <div class="testimonial-item">
                {{-- <img src="/storage/{{ $item->user->foto }}" alt="" width="200" class="rounded-circle"> --}}
                <img src="/storage/{{ $item->user->foto }}" class="testimonial-img" alt="" width="100">
                <h3>{{ $item->user->name }}</h3>
                <h4>{{ $item->user->nisn }}</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  {{$item->testimoni}}
                  {{-- Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper. --}}
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div> {{-- <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="app/assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
                <h3>Sara Wilsson</h3>
                <h4>Designer</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="app/assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
                <h3>Jena Karlis</h3>
                <h4>Store Owner</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="app/assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
                <h3>Matt Brandon</h3>
                <h4>Freelancer</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="app/assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
                <h3>John Larson</h3>
                <h4>Entrepreneur</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div> --}}

            </div>
            
            @endforeach
           
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

     <!-- ======= Informasi Section ======= -->
     <section id="informasi" class="testimonials section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Informasi</h2>
          {{-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> --}}
        </div>

        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">
            @foreach ($posting as $item )
                
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="/storage/{{ $item->foto }}" class="testimonial-img" alt="">
                <h3>{{ $item->lowongan }}</h3>
                <h4>{{ $item->ttl_p }}</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  {{ $item->deskripsi }}
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                {{-- <a href="/posting/detail/{{ $item->id_posting}}" class="get-started-btn" data-toggle="tooltip" title="Detail">Detail</a> --}}
                <a href="/posting/detail/{{ $item->id_posting}}" class="btn section-bg btn-action mr-1"  data-toggle="tooltip" title="Selengkapnya" style="background: rgb(38, 107, 211))0, 51, 255)">Selengkapnya</a>
                {{-- <button></button> --}}
              </div>
            </div>
            
            @endforeach
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

    
    <!-- ======= Pricing Section ======= -->
    <section class="pricing" id="lowongan">
      <div class="container">

        <div class="section-title">
          <h2>Lowongan</h2>
          {{-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> --}}
        </div>
        <a href="{{ url('pengajuan/add')}}" class="get-started-btn">Daftar</a>

        <div class="row no-gutters">
          @foreach ($pengajuan as $item)
              
          <div class="col-lg-4 box" data-aos="fade-right">
            <img alt="image" src="/storage/{{ $item->foto }}" class="rounded-circle profile-widget-picture" width="130" height="100">
            <h4><span>{{ $item->nama_lengkap }}</span></h4>
            <ul>
              {{-- <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>
              <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li> --}}
              {{-- <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>
              <li class="na"><i class="bx bx-x"></i> <span>Pharetra massa massa ultricies</span></li>
              <li class="na"><i class="bx bx-x"></i> <span>Massa ultricies mi quis hendrerit</span></li> --}}
            </ul>
            <a href="/pengajuan/detail/{{ $item->id_pengajuan}}" class="get-started-btn">Detail</a>
            <a href="/testimoni/add/{{ $item->id_pengajuan }}"><button class="btn btn-info section-bg get-started-btn">Tambah Testimoni</button></a>
          </div>
          
          @endforeach

          {{-- <div class="col-lg-4 box" data-aos="fade-left">
            <h3>Developer</h3>
            <h4>$49<span>per month</span></h4>
            <ul>
              <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>
              <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>
              <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>
              <li><i class="bx bx-check"></i> Pharetra massa massa ultricies</li>
              <li><i class="bx bx-check"></i> Massa ultricies mi quis hendrerit</li>
            </ul>
            <a href="#" class="get-started-btn">Get Started</a>
          </div> --}}

        </div>

      </div>
    </section><!-- End Pricing Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contact</h2>
          {{-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> --}}
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-6 info">
                <i class="bx bx-map"></i>
                <h4>Address</h4>
                <p>A108 Adam Street,<br>New York, NY 535022</p>
              </div>
              <div class="col-lg-6 info">
                <i class="bx bx-phone"></i>
                <h4>Call Us</h4>
                <p>+1 5589 55488 55<br>+1 5589 22548 64</p>
              </div>
              <div class="col-lg-6 info">
                <i class="bx bx-envelope"></i>
                <h4>Email Us</h4>
                <p>contact@example.com<br>info@example.com</p>
              </div>
              <div class="col-lg-6 info">
                <i class="bx bx-time-five"></i>
                <h4>Working Hours</h4>
                <p>Mon - Fri: 9AM to 5PM<br>Sunday: 9AM to 1PM</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <form action="{{ url('/kontak/create') }}" method="post"  class="php-email-form">
              @csrf
              <div class="form-group">
                <input placeholder="Your Name" type="text" name="nama" class="form-control" id="nama" required>
              </div>
              <div class="form-group mt-3">
                <input placeholder="Your Email" type="email" class="form-control" name="email" id="email" required>
              </div>
              {{-- <div class="form-group mt-3">
                <input placeholder="Subject" type="text" class="form-control" name="subject" id="subject" required>
              </div> --}}
              <div class="form-group mt-3">
                <textarea placeholder="Message" class="form-control" name="pesan" rows="5" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
  
            {{-- <h4>Join Our Newsletter</h4> --}}
            {{-- <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p> --}}
            {{-- <form action="" method="post"> --}}
              {{-- <input type="email" name="email"><input type="submit" value="Subscribe"> --}}
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>BKK SMK YPC</h3>
            <p>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> info@example.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Tentang Kami</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Perusahaan</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Lowongan</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contact</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul class="py-1">
              {{-- <li><i class="bx bx-chevron-right"></i> --}}
                <p>SMK YPC Tasikmalaya</p>
                <p>Jawa Barat, Indonesia</p>
                <p>Phone: +1 5589 55488 55</p>
                <p>Email: info@example.com</p>
                
              {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li> --}}
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
             <h4>Our Social Networks</h4>
            {{-- <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p> --}}
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container py-4">
      {{-- <div class="copyright"> --}}
        Smk Ypc Tasikmalaya <strong><span> </span></strong>. All Rights Reserved
      {{-- </div> --}}
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-app-landing-page-template/ -->
        Designed by <a href="https://bootstrapmade.com/">Elsa Novianti</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('app/assets/vendor/aos/aos.js')}}"></script>
  <script src="{{ asset('app/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('app/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>
  <script src="{{ asset('app/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{ asset('app/assets/vendor/php-email-form/validate.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('app/assets/js/main.js')}}"></script>

</body>
</html>