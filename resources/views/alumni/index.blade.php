<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>General Dashboard &mdash; Stisla</title>

  <!-- Favicons -->
<link href="{{ asset('img/logo smk.png')}}" rel="icon">
<link href="{{ asset('img/logo smk.png')}}" rel="apple-touch-icon">
<link rel="shortcut icon" href="img/logo smk.png" type="image/x-icon">

  <!-- General CSS Files -->
  <link rel="stylesheet" href="adm/assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="adm/assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="adm/assets/modules/jqvmap/dist/jqvmap.min.css">
  <link rel="stylesheet" href="adm/assets/modules/weather-icon/css/weather-icons.min.css">
  <link rel="stylesheet" href="adm/assets/modules/weather-icon/css/weather-icons-wind.min.css">
  <link rel="stylesheet" href="adm/assets/modules/summernote/summernote-bs4.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="adm/assets/css/style.css">
  <link rel="stylesheet" href="adm/assets/css/components.css">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar" style="background: rgb(65, 65, 230)">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
            <div class="search-result">
    
            </div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="adm/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, Adminstrator</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="http://127.0.0.1:8000/tampilan/profile" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <div class="dropdown-divider"></div>
              <a href="http://127.0.0.1:8000/hm" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href=""></a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="">St</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="dropdown active">
              <a href="http://127.0.0.1:8000/stisla"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Pages</li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Admin</span></a>
              <ul class="dropdown-menu">
                <li><a href="http://127.0.0.1:8000/admin1">Data Admin</a></li> 
              </ul>
            </li>
            <li><a class="nav-link  has-dropdown" data-toggle="dropdown"><i class="far fa-square"></i> <span>Alumni</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="http://127.0.0.1:8000/alumni">Data Alumni</a></li>
                  {{-- <li><a class="nav-link" href="layout-transparent.html">Tambah Lamaran</a></li> --}}
                </ul></li>
            </li>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Perusahaan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://127.0.0.1:8000/perusahaan">Data Perusahaan</a></li>
                {{-- <li><a class="nav-link" href="perusahaan/add">Tambah Perusahaan</a></li> --}}
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Posting</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://127.0.0.1:8000/posting">Data Posting</a></li>
                {{-- <li><a class="nav-link" href="posting/add">Tambah Posting</a></li> --}}
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Pengajuan</span></a>
              <ul class="dropdown-menu">
                <li><a href="http://127.0.0.1:8000/pengajuan">Data Pengajuan</a></li>
                {{-- <li><a class="nav-link" href="utilities-invoice.html">Tambah Pengajuan</a></li> --}}
              </ul>
            </li> 
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fa fa-comment" aria-hidden="true"></i><span>Kontak</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://localhost:8000/kontak">Data Kontak</a></li>
                {{-- <li><a class="nav-link" href="perusahaan/add">Tambah Perusahaan</a></li> --}}
              </ul>
            </li>    
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i><span>Testimoni</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://127.0.0.1:8000/testimoni">Data Testimoni</a></li>
                {{-- <li><a class="nav-link" href="perusahaan/add">Tambah Perusahaan</a></li> --}}
              </ul>
            </li>            
          </ul>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h3>Data Alumni</h3>
            {{-- <a class="" href="{{ url('posting/add')}}"><button class="btn btn-primary" style="background: blue">Tambah Posting</button></a> --}}
            @csrf
          </div>   
         
          <div class="row">
            {{-- <div class="col-lg-7 col-md-12 col-12 col-sm-12"> --}}
              <div class="card">
                <div class="card-header">
                  <h4>Alumni</h4>
                  <div class="card-header-action">
                    <a href="#" class="btn btn-primary">View All</a>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table table-striped mb-0">
                      
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nisn</th>
                          <th>Nama</th>
                          <th>KTP</th>
                          <th>Tanggal_lahir</th>
                          <th>Alamat</th>
                          {{-- <th>Level</th> --}}
                          <th>Foto</th>
                          <th>No Hp</th>
                          <th>Tahun Lulus</th>
                          <th>Email</th>
                          <th>Password</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>    
                        @foreach ($user as $key => $item)
                        <tr>
                          <td>{{ $key+1 }}</td>
                          <td>{{ $item->nisn }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->ktp }}</td>
                          <td>{{ $item->tanggal_lahir }}</td>
                          <td>{{ $item->alamat }}</td>
                          {{-- <td>{{ $item->level }}</td> --}}
                          <td>
                            <a class="font-weight-600"><img src="/storage/{{ $item->foto }}" alt="avatar" width="30" class="rounded-circle mr-1"></a>
                           </td>
                          <td>{{ $item->no_hp }}</td>
                          <td>{{ $item->ttn_lulus }}</td>
                         
                          <td>{{ $item->email }}</td>
                          <td>{{ $item->password }}</td>
                          <td>
                            <a href="/user/profile/{{ $item->nisn}}" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Detail"><i class="fas fa-ellipsis-v"></i></a>
                            <a href="/user/edit/{{ $item->nisn}}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                            <a href="/user/hapus/{{ $item->nisn }}" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete" data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?" ><i class="fas fa-trash"></i></a>
                          </td>
                        </tr>
                        @endforeach
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Smk Ypc Tasikmalaya  2023 <div class="bullet"></div> Design By <a href="">Elsa Novianti</a>
        </div>
        <div class="footer-right">
          
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="adm/assets/modules/jquery.min.js"></script>
  <script src="adm/assets/modules/popper.js"></script>
  <script src="adm/assets/modules/tooltip.js"></script>
  <script src="adm/assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="adm/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="adm/assets/modules/moment.min.js"></script>
  <script src="adm/assets/js/stisla.js"></script>
  
  <!-- JS Libraies -->
  <script src="adm/assets/modules/simple-weather/jquery.simpleWeather.min.js"></script>
  <script src="adm/assets/modules/chart.min.js"></script>
  <script src="adm/assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
  <script src="adm/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
  <script src="adm/assets/modules/summernote/summernote-bs4.js"></script>
  <script src="adm/assets/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

  <!-- Page Specific JS File -->
  <script src="adm/assets/js/page/index-0.js"></script>
  
  <!-- Template JS File -->
  <script src="adm/assets/js/scripts.js"></script>
  <script src="adm/assets/js/custom.js"></script>
</body>
</html>