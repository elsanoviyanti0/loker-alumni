<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Dashboard</title>

  <!-- Favicons -->
<link href="{{ asset('img/logo smk.png')}}" rel="icon">
<link href="{{ asset('img/logo smk.png')}}" rel="apple-touch-icon">
<link rel="shortcut icon" href="img/logo smk.png" type="image/x-icon">

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/jqvmap/dist/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons.min.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/modules/weather-icon/css/weather-icons-wind.min.css')}}">
  <link rel="stylesheet" href="{{ asset('adm/assets/modules/summernote/summernote-bs4.css')}}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('adm/assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('adm/assets/css/components.css')}}">
  <link rel="shortcut icon" href="{{asset('img/logo smk.png" type="image/x-icon')}}">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar" style="background: rgb(65, 65, 230)">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
            <div class="search-result">
            </div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="adm/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, Adminstrator</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="http://127.0.0.1:8000/tampilan/profile" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <div class="dropdown-divider"></div>
              <a href="http://127.0.0.1:8000/logout" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href=""></a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href=""></a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="dropdown active">
              <a href="http://127.0.0.1:8000/stisla"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Pages</li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Admin</span></a>
              <ul class="dropdown-menu">
                <li><a href="http://127.0.0.1:8000/admin1">Data Admin</a></li> 
              </ul>
            </li>
            <li><a class="nav-link  has-dropdown" data-toggle="dropdown"><i class="far fa-square"></i> <span>Alumni</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="http://127.0.0.1:8000/alumni">Data Alumni</a></li>
                  {{-- <li><a class="nav-link" href="layout-transparent.html">Tambah Lamaran</a></li> --}}
                </ul></li>
            </li>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Perusahaan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://127.0.0.1:8000/perusahaan">Data Perusahaan</a></li>
                {{-- <li><a class="nav-link" href="perusahaan/add">Tambah Perusahaan</a></li> --}}
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Posting</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://127.0.0.1:8000/posting">Data Posting</a></li>
                {{-- <li><a class="nav-link" href="posting/add">Tambah Posting</a></li> --}}
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Pengajuan</span></a>
              <ul class="dropdown-menu">
                <li><a href="http://127.0.0.1:8000/pengajuan">Data Pengajuan</a></li>
                {{-- <li><a class="nav-link" href="utilities-invoice.html">Tambah Pengajuan</a></li> --}}
              </ul>
            </li> 
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fa fa-comment" aria-hidden="true"></i><span>Kontak</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://localhost:8000/kontak">Data Kontak</a></li>
                {{-- <li><a class="nav-link" href="perusahaan/add">Tambah Perusahaan</a></li> --}}
              </ul>
            </li>    
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i><span>Testimoni</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="http://127.0.0.1:8000/testimoni">Data Testimoni</a></li>
                {{-- <li><a class="nav-link" href="perusahaan/add">Tambah Perusahaan</a></li> --}}
              </ul>
            </li>            
          </ul>
      </div>


      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Profile</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
              <div class="breadcrumb-item">Profile</div>
            </div>
          </div>
          <div class="section-body">
            {{-- @foreach ($admin as $item) --}}
                
            <h2 class="section-title">Hi, {{ $user->name }}!</h2>
            <p class="section-lead">
              {{-- Change information about yourself on this page. --}}
            </p>
            
            <div class="row mt-sm-4">
              <div class="col-12 col-md-12 col-lg-5">
                <div class="card profile-widget">
                  <div class="profile-widget-header">                     
                    <img alt="image" src="/storage/{{ $user->foto }}" class="rounded-circle profile-widget-picture" width="120" height="120">
                    <div class="profile-widget-items">
                    </div>
                  </div>
                  <div class="profile-widget-description">
                    <div class="profile-widget-name">{{ $user->name }} <div class="text-muted d-inline font-weight-normal"><div class="slash"></div>{{ $user->email }}</div></div>
                    {{-- Ujang maman is a superhero name in <b>Indonesia</b>, especially in my family. He is not a fictional character but an original hero in my family, a hero for his children and for his wife. So, I use the name as a user in this template. Not a tribute, I'm just bored with <b>'John Doe'</b>. --}}
                  </div>
                  <div class="card-footer text-center">
                    {{-- <div class="font-weight-bold mb-2">Follow Ujang On</div>
                    <a href="#" class="btn btn-social-icon btn-facebook mr-1">
                      <i class="fab fa-facebook-f"></i>
                    </a> --}}
                    <a href="#" class="btn btn-social-icon btn-twitter mr-1">
                      <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-github mr-1">
                      <i class="fab fa-github"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-instagram">
                      <i class="fab fa-instagram"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-7">
                <div class="card">
                  <form method="post" class="needs-validation" novalidate="">
                    <div class="card-header">
                      <h4>Profile</h4>
                    </div>
                    <div class="card-body">
                          
                      <div class="row">    
                        <div class="form-group col-md-6 col-12">
                          <label>NISN</label>
                          <input type="text" class="form-control" value=" {{ $user->nisn }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>                           
                        <div class="form-group col-md-6 col-12">
                          <label>Nama</label>
                          <input type="text" class="form-control" value=" {{ $user->name }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>KTP</label>
                          <input type="text" class="form-control" value=" {{ $user->ktp }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Tanggal Lahir</label>
                          <input type="text" class="form-control" value=" {{ $user->tanggal_lahir }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>No Hp</label>
                          <input type="text" class="form-control" value=" {{ $user->no_hp }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Alamat</label>
                          <input type="text" class="form-control" value=" {{ $user->alamat }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        {{-- <div class="form-group col-md-6 col-12">
                          <label>Level</label>
                          <input type="text" class="form-control" value=" {{ $user->level }}">
                          <div class="invalid-feedback">
                          </div>
                        </div> --}}
                        <div class="form-group col-md-6 col-12">
                          <label>Tahun Lulus</label>
                          <input type="text" class="form-control" value=" {{ $user->ttn_lulus }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Email</label>
                          <input type="email" class="form-control" value=" {{ $user->email }}">
                          <div class="invalid-feedback">
                          </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                          <label>Password</label>
                          <input type="text" class="form-control" value=" {{ $user->password }}">
                          <div class="invalid-feedback">
    
                          </div>
                        </div>
                      </div>
                      {{-- @endforeach --}}
                      {{-- <div class="row">
                          <div class="form-group col-12">
                            <label>Bio</label>
                            <textarea class="form-control summernote-simple">Ujang maman is a superhero name in <b>Indonesia</b>, especially in my family. He is not a fictional character but an original hero in my family, a hero for his children and for his wife. So, I use the name as a user in this template. Not a tribute, I'm just bored with <b>'John Doe'</b>.</textarea>
                          </div>
                        </div> --}}
                        <div class="row">
                          <div class="form-group mb-0 col-12">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" name="remember" class="custom-control-input" id="newsletter">
                              {{-- <label class="custom-control-label" for="newsletter">Subscribe to newsletter</label> --}}
                              <div class="text-muted form-text">
                                {{-- You will get new information about products, offers and promotions --}}
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                      {{-- <button class="btn btn-primary">Save Changes</button> --}}
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Smk Ypc Tasikmalaya  2023 <div class="bullet"></div> Design By <a href="">Elsa Novianti</a>
        </div>
        <div class="footer-right">
          
        </div>
      </footer>
    </div>
  </div>

  <script src="{{ asset('adm/assets/modules/jquery.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/popper.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/tooltip.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('adm/assets/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/moment.min.js')}}"></script>
  <script src="{{ asset('adm/assets/js/stisla.js')}}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('adm/assets/modules/simple-weather/jquery.simpleWeather.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chart.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/jquery.vmap.min.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/summernote/summernote-bs4.js')}}"></script>
  <script src="{{ asset('adm/assets/modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>

  <!-- Page Specific JS File -->
  <script src="{{ asset('adm/assets/js/page/index-0.js')}}"></script>
  
  <!-- Template JS File -->
  <script src="{{ asset('adm/assets/js/scripts.js')}}"></script>
  <script src="{{ asset('adm/assets/js/custom.js')}}"></script>

</body>
</html>