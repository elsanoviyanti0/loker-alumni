<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengajuan', function (Blueprint $table) {
            $table->increments('id_pengajuan');
            $table->string('nisn');
            $table->unsignedInteger('id_posting');
            $table->string('nama_lengkap');
            $table->date('ttl');
            $table->text('tempat');
            $table->text('alamat');
            $table->string('no_hp',13);
            $table->string('email');
            $table->text('foto');
            $table->text('dokumen');
            $table->date('ttl_p');
            $table->string('status');
            $table->foreign('nisn')->references('nisn')->on('users')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('id_posting')->references('id_posting')->on('posting')->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengajuan');
    }
};
