<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('posting', function (Blueprint $table) {
            $table->increments('id_posting');
            $table->string('nisn');
            $table->unsignedInteger('id_admin');
            $table->unsignedInteger('id_perusahaan');
            // $table->string('nama');
            $table->string('bidang_usaha',50);
            $table->string('persyaratan',50);
            $table->string('lowongan',50);
            $table->date('ttl_p');
            // $table->date('ttl_tp');
            $table->text('deskripsi');
            $table->text('foto');
            $table->text('lokasi');
            $table->foreign('nisn')->references('nisn')->on('users')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('id_admin')->references('id_admin')->on('admin')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('id_perusahaan')->references('id_perusahaan')->on('perusahaan')->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('posting');
    }
};
