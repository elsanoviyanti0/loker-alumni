<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AlumniController;
use App\Http\Controllers\PengajuanController;
use App\Http\Controllers\PostingController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PerusahaanController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\User1Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TampilController;
use App\Http\Controllers\KontakController;
use App\Http\Controllers\TestimoniController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view('footer.login');
})->name('login');


Route::get('/tivo', function () {
    return view('layout.tivo');
});

Route::get('beranda', function () {
    return view('layout.landingpage');
});

Route::get('dashboard', function () {
    return view('layout.dashboard');
});

Route::get('form', function () {
    return view('inp');
});

// Route::middleware(['isuser'])->group(function (){
    Route::get('/testimoni/add/{id}',[TestimoniController::class,'add_testimoni']);
    Route::post('/testimoni/create/{id}',[TestimoniController::class,'create_testimoni']);

    Route::get('expired-date', [TampilController::class, 'date']);

    Route::get('posting/detail/{id_posting}', [TampilController::class, 'informasi']);
    Route::get('pst/detail/{id_posting}', [PostingController::class, 'pst']);

    Route::get('pengajuan/detail/{id_pengajuan}', [TampilController::class, 'detail']);
    Route::get('all', [TampilController::class, 'posting']);
    Route::get('landingpage', [TampilController::class, 'lading']);
    Route::get('tampilan', [TampilController::class, 'show']);
    Route::get('tampilan/profile', [TampilController::class, 'us']);
    Route::get('user/profile/{nisn}', [TampilController::class, 'profile']);

    
    
    Route::get('lading', [HomeController::class, 'show']);
    Route::get('index', [HomeController::class, 'from']);
    Route::get('tmb', [HomeController::class, 'index']);
    Route::get('layoute', [HomeController::class, 'lay']);
    Route::get('stisla', [HomeController::class, 'das']);
    
    Route::get('/alumni', [User1Controller::class, 'view']);
// });

Route::get('navbar', [HomeController::class, 'nav']);
Route::get('user', [HomeController::class, 'profile']);
Route::get('hm', [HomeController::class, 'home']);

// Route::middleware(['isGuest'])->group(function (){
    // Buat Akun Pengguna
    Route::post('/login', [User1Controller::class, 'show']);
    Route::get('/daftar', [User1Controller::class, 'show']);
    Route::post('/daftar/create', [User1Controller::class, 'create']);
    Route::post('/auth', [User1Controller::class, 'auth']);
    Route::get('/logout', [User1Controller::class, 'logout']);
// });

//Admin
Route::get('/login/admin', [AdminController::class, 'login_adm']);
Route::post('/login/admin', [AdminController::class, 'auth']);
Route::post('/logout/admin', [AdminController::class, 'logout']);

Route::get('/ivo', [AdminController::class, 'lyt']);

Route::get('/admin1', [AdminController::class, 'show']);


// Route::middleware(['isAdmin'])->group(function (){

    // Testimoni
    Route::get('/testimoni',[TestimoniController::class,'testimoni']);
    Route::get('testimoni',[TestimoniController::class,'index']);
    Route::get('testimoni/delete/{id}',[TestimoniController::class,'delete']);

    // Perusahaan
    Route::get('/perusahaan', [PerusahaanController::class, 'show']);
    Route::get('/perusahaan/add', [PerusahaanController::class, 'add']);
    Route::post('/perusahaan/create', [PerusahaanController::class, 'create']);
    Route::get('/perusahaan/edit/{id}', [PerusahaanController::class, 'edit']);
    Route::post('/perusahaan/update/{id}', [PerusahaanController::class, 'update']);
    Route::get('/perusahaan/hapus/{id}', [PerusahaanController::class, 'hapus']);

    // Posting
    Route::get('/posting', [PostingController::class, 'show']);
    Route::get('/posting/add', [PostingController::class, 'add']);
    Route::post('/posting/create', [PostingController::class, 'create']);
    Route::get('/posting/edit/{id}', [PostingController::class, 'edit']);
    Route::post('/posting/update/{id}', [PostingController::class, 'update']);
    Route::get('/posting/hapus/{id}', [PostingController::class, 'hapus']);

    Route::get('/posting/list', [PostingController::class, 'postingList'])->name('posting-list'); 

    // Pengajuan
    Route::get('/admin', [PengajuanController::class, 'adm']);

    Route::get('/accetp/{id_pengajuan}', [PengajuanController::class, 'acc']);
    Route::get('/reject/{id_pengajuan}', [PengajuanController::class, 'rejec']);

    Route::get('/pengajuan', [PengajuanController::class, 'show']);
    Route::get('/pengajuan/add', [PengajuanController::class, 'add']);
    Route::post('/pengajuan/create', [PengajuanController::class, 'create']);
    Route::get('/pengajuan/edit/{id}', [PengajuanController::class, 'edit']);
    Route::post('/pengajuan/update/{id}', [PengajuanController::class, 'update']);
    Route::get('/pengajuan/hapus/{id}', [PengajuanController::class, 'hapus']);

    // Kontak
    Route::get('/kontak', [KontakController::class, 'show']);
    Route::get('/kontak/add', [KontakController::class, 'add']);
    Route::post('/kontak/create', [KontakController::class, 'create']);
    Route::get('/kontak/hapus/{id}', [KontakController::class, 'hapus']);

    Route::get('/kontak/accetp/{id}', [KontakController::class, 'acc']);
    Route::get('/kontak/reject/{id}', [KontakController::class, 'rejec']);
// });